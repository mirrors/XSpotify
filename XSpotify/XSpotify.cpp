﻿// ==========================================================
// Project: XSpotify
// 
// Component: XSpotify.dll
//
// Purpose: Entry point
//          
// Initial author: Meik Wochnik
//
// Started: 14.10.2019
// ==========================================================

#include ".\\\include\BaseInclude.hpp"

void PatchSpotify()
{
	Modules::Initialization::Initialization();
	Modules::Downloads::Downloads();
	Modules::AutoUpdates::AutoUpdates();
	Modules::Ads::Ads();
	//XPatchBitrate();
}

DWORD WINAPI Exec(LPVOID lpParam)
{
	AllocConsole();
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	PatchSpotify();
	return 0;
}

BOOL WINAPI DllMain(HINSTANCE hModule, DWORD dwAttached, LPVOID lpvReserved)
{
	if (dwAttached == DLL_PROCESS_ATTACH)
	{
		CreateThread(NULL, 0, &Exec, NULL, 0, NULL);
	}
	return 1;
}